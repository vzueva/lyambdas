package main;

import art.intelligence.global.CommiGlobalSearch;
import art.intelligence.global.GlobalSearch;
import art.intelligence.local.AStarLocalSearch;
import art.intelligence.local.LocalSearch;
import entity.*;
import entity.MapExt;
import entity.Position;
import entity.RobotMovement;
import game.GameController;
import java.awt.Color;
import java.awt.Insets;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.FileInputStream;
import map.generator.MapGenerator;
import map.generator.UpdateFlags;
import map.loader.MapLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import map.utils.MapUtils;

import javax.swing.Timer;

import java.util.LinkedList;
import java.util.List;
import main.java.automover.AutoMover;

public class Main extends JFrame implements KeyListener {
    
    public static JTextField typingArea;
    public static char[][] mapArr;
    public static int score = 0, lyambdas = 0, lyambdascount = 0;
    public static int robotPosY, robotPosX;
    public static JFrame frame = new JFrame();
    public static JPanel panel = new JPanel();
    public static JPanel startPanel = new JPanel();
    public static JPanel autoPanel = new JPanel();
    public static JPanel finalPanel = new JPanel();
    
    
    public static JButton autoShowButton = new JButton(new ImageIcon(Main.class.getClassLoader().getResource("main-button-auto-show.jpg")));
    public static String fileName = "./map5.txt";
    
    public static MapExt map;
    public static MapExt mapAuto;
    
    public static GameStatus gs = GameStatus.IN_PROGRESS;
    public static GameController gc = new GameController();
    public static String autoMoves = "";
    
    public static int autoMoveCounter = -1;
    public static Timer timer;
    


    public static void main(String[] args) throws IOException {
        new Main();
//        String fileName = "./map4.txt";
//	GameController game_controll = new GameController();
//        MapExt map = new MapExt(MapLoader.loadMapFromFile(fileName));
//        System.out.println(map);
//        System.out.println("==========================================");
//
//        GlobalSearch globalSearch = new CommiGlobalSearch();
//        List<Position> lambdasSequence = globalSearch.defineLambdasSequence(map);
//        System.out.print("\nResults of the Global Search: \n");
//        int div = 6;
//        for(int i=0; i<lambdasSequence.size(); i++){
//            if(i==0) System.out.print(lambdasSequence.get(i));
//            else if (i%div==0){
//                System.out.print("->");
//                System.out.print("\n");
//                System.out.print(" -> "+lambdasSequence.get(i));
//            }
//            else System.out.print(" -> "+lambdasSequence.get(i));
//        }
//        System.out.print("\n\n");
//        System.out.println("==========================================");
//        LocalSearch localSearch = new AStarLocalSearch();
//
//        while (!lambdasSequence.isEmpty()) {
//            Position nextLambda = lambdasSequence.get(0);
//            System.out.println("Next lambda: " + nextLambda);
//            Position currentRobotPosition = GameController.detectRobotPosition(map);
//            localSearch.rebuildPath(map, currentRobotPosition, nextLambda);
//            do {
//                System.out.println("\t Robot: " + currentRobotPosition);
//                RobotMovement nextMovement = localSearch.defineNextRobotMovement(currentRobotPosition);
//                System.out.println("\t Movement: " + nextMovement);
//                UpdateFlags flags = MapGenerator.updateMap(map, nextMovement);
//                System.out.println(map);
//                currentRobotPosition = GameController.detectRobotPosition(map);
//
//                if (flags.isUpdateLocalSearch()) {
//                    localSearch.rebuildPath(map, currentRobotPosition, nextLambda);
//                }
//            } while (!currentRobotPosition.equals(nextLambda));
//
//            lambdasSequence = globalSearch.defineLambdasSequence(map);
//        }
    }
    
    public Main(int i){
        
        int quantityMoves = autoMoves.length();
        
        if(autoMoveCounter>=0){ 
            timer.stop();
            
            frame.remove(autoPanel);
            
            autoPanel = new JPanel();
            autoPanel.setSize(31*mapAuto.getCols(), 31);
            
            frame.remove(panel);
            panel = new JPanel();
            panel.setLayout(new FlowLayout(FlowLayout.LEFT,1,1));
            panel.setBackground(Color.LIGHT_GRAY); 


            char bukva = autoMoves.charAt(autoMoveCounter);

            if(bukva=='L'){
                gs = gc.acceptMove(mapAuto, RobotMovement.LEFT);
                UpdateFlags flags = MapGenerator.updateMap(mapAuto, RobotMovement.LEFT);
                autoPanel.add(new JLabel("Move: Left"));
            }else if(bukva=='D'){
                gs = gc.acceptMove(mapAuto, RobotMovement.DOWN);
                UpdateFlags flags = MapGenerator.updateMap(mapAuto, RobotMovement.DOWN);
                autoPanel.add(new JLabel("Move: Down"));
            }else if(bukva=='U'){
                gs = gc.acceptMove(mapAuto, RobotMovement.UP);
                UpdateFlags flags = MapGenerator.updateMap(mapAuto, RobotMovement.UP);
                autoPanel.add(new JLabel("Move: Up"));
            }else if(bukva=='R'){
                gs = gc.acceptMove(mapAuto, RobotMovement.RIGHT);
                UpdateFlags flags = MapGenerator.updateMap(mapAuto, RobotMovement.RIGHT);
                autoPanel.add(new JLabel("Move: Right"));
            }else if(bukva=='A'){
                autoPanel.add(new JLabel("Result: Abort"));
                gs = gc.acceptMove(mapAuto, RobotMovement.ABORT);
            }else if(bukva=='W'){
                gs = gc.acceptMove(mapAuto, RobotMovement.WAIT);
                UpdateFlags flags = MapGenerator.updateMap(mapAuto, RobotMovement.WAIT);
                autoPanel.add(new JLabel("Move: Wait"));
            }
            
            for(int x=0; x<mapAuto.getCols(); x++){
                panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("free.png"))));
            }

            for(int y=mapAuto.getRows()-1; y>=0; y--){
                for(int x=0; x<mapAuto.getCols(); x++){
                    if(mapAuto.at(y,x)==MineLayout.ROCK){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("bomb.png")))); }

                    else if(mapAuto.at(y,x)==MineLayout.LAMBDA){ 
                        panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("grape.png"))));
                        lyambdas += 1;
                    }
                    else if(mapAuto.at(y,x)==MineLayout.EMPTY){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("free.png")))); }
                    else if(mapAuto.at(y,x)==MineLayout.LIFT_CLOSED){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("gates.png")))); }
                    else if(mapAuto.at(y,x)==MineLayout.LIFT_OPENED){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("gatesopen.png")))); }
                    else if(mapAuto.at(y,x)==MineLayout.SOIL){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("herb.png")))); }
                    else if(mapAuto.at(y,x)==MineLayout.ROBOT){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("robot.png")))); }
                    else if(mapAuto.at(y,x)==MineLayout.WALL){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("bricks.png")))); }
                    else { panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("free.png")))); }
                }
            }
            
            
            autoPanel.add(new JLabel("Score: "+gc.getScore()));
            int autoMoveCounterPlus = autoMoveCounter+1;
            autoPanel.add(new JLabel("Move number: "+ autoMoveCounterPlus + "/" + quantityMoves));
                
            frame.add(autoPanel);
            frame.add(panel);
            frame.setSize(31*mapAuto.getCols()+7, 31*mapAuto.getRows()+60);
            frame.revalidate();
            frame.repaint();
        }
        
        autoMoveCounter++;
        
        if(autoMoveCounter<quantityMoves){ 
        
          ActionListener reload = new ActionListener() {
              public void actionPerformed(ActionEvent evt) {
                  new Main(1); 
              }
          };
              timer = new Timer(250, reload);
              timer.start();
          
        }else{ timer.stop(); }
        
    }
    
    public Main() throws IOException  { //����� ���� � �������� �����
        
        map = new MapExt(MapLoader.loadMapFromFile(fileName));
        mapAuto = new MapExt(MapLoader.loadMapFromFile(fileName));
        
        typingArea = new JTextField();
        typingArea.addKeyListener(this);
        typingArea.setSize(0, 0);
        
        frame.setTitle("Lyambda lifter");
        frame.setResizable(false);
        
        

        frame.setLocation(100, 100);
        frame.setSize(400, 220);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        startPanel.setBackground(Color.white); 
        startPanel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("main.png"))));  
        
        panel.setBackground(Color.LIGHT_GRAY);  
        panel.setLayout(new FlowLayout(FlowLayout.LEFT,1,1));
        
        autoPanel.setBackground(Color.white);
        finalPanel.setBackground(Color.white);
        
        
        JButton handButton = new JButton(new ImageIcon(Main.class.getClassLoader().getResource("main-button-hand.jpg")));
        handButton.setMargin(new Insets(-3, -3, -3, -3));
        startPanel.add(handButton);

        JButton autoButton = new JButton(new ImageIcon(Main.class.getClassLoader().getResource("main-button-auto.jpg")));
        autoButton.setMargin(new Insets(-3, -3, -3, -3));
        startPanel.add(autoButton);
        
        
        
        
        frame.add(startPanel);
        
        frame.revalidate();
        frame.repaint();
        
        //Interface is created
        
        
        handButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                frame.add(typingArea);
                
                for(int x=0; x<mapAuto.getCols(); x++){
                    panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("free.png"))));
                }
                
                for(int y=map.getRows()-1; y>=0; y--){
                    for(int x=0; x<map.getCols(); x++){
                        if(map.at(y,x)==MineLayout.ROCK){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("bomb.png")))); }
                        
                        else if(map.at(y,x)==MineLayout.LAMBDA){ 
                            panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("grape.png")))); 
                            lyambdas += 1;
                        }
                        else if(map.at(y,x)==MineLayout.EMPTY){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("free.png")))); }
                        else if(map.at(y,x)==MineLayout.LIFT_CLOSED){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("gates.png")))); }
                        else if(map.at(y,x)==MineLayout.LIFT_OPENED){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("gatesopen.png")))); }
                        else if(map.at(y,x)==MineLayout.SOIL){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("herb.png")))); }
                        else if(map.at(y,x)==MineLayout.ROBOT){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("robot.png")))); }
                        else if(map.at(y,x)==MineLayout.WALL){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("bricks.png")))); }
                        else { panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("free.png")))); }
                    }
                }
                
                frame.remove(startPanel);
                
                autoPanel = new JPanel();
                autoPanel.setSize(31*mapAuto.getCols(), 31);
                
                autoPanel.add(new JLabel("Score: "+gc.getScore()));
                
                frame.add(autoPanel);
            
                frame.add(panel);
                frame.setSize(31*map.getCols()+7, 31*map.getRows()+60);
                frame.revalidate();
                frame.repaint();
                
            }
        });
        
        autoButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                frame.remove(startPanel);
                autoPanel = new JPanel();
                autoPanel.setSize(31*mapAuto.getCols(), 31);
        
                autoPanel.add(new JLabel("Result:" + gs));
                autoPanel.add(new JLabel("Score:" + gc.getScore()));
                
                frame.add(autoPanel);
                frame.revalidate();
                frame.repaint();
                
		AutoMover am = new AutoMover(map);
		GameStatus gs = am.startAIWayBuilder();
		autoMoves=am.getWay();
//                GameController game_controll = new GameController();
//
////                System.out.println(map);
//                System.out.println("==========================================");
//
//		List<Position> unreach_L_list = new LinkedList<Position>(); //List with positions of unreachable lyambdas
//                GlobalSearch globalSearch = new CommiGlobalSearch();
//                List<Position> lambdasSequence = null;
////		globalSearch.defineLambdasSequence(map, unreach_L_list);
////                System.out.print("\nResults of the Global Search: \n");
////                int div = 6;
////                for (int i = 0; i < lambdasSequence.size(); i++) {
////                    if (i == 0) System.out.print(lambdasSequence.get(i));
////                    else if (i % div == 0) {
////                        System.out.print("->");
////                        System.out.print("\n");
////                        System.out.print(" -> " + lambdasSequence.get(i));
////                    } else System.out.print(" -> " + lambdasSequence.get(i));
////                }
////                System.out.print("\n\n");
////                System.out.println("==========================================");
//                LocalSearch localSearch = new AStarLocalSearch();
//                GameStatus gs = GameStatus.IN_PROGRESS;
//                GameController gc = new GameController();
//                boolean one_step_wait=true;
//                while (gs == GameStatus.IN_PROGRESS) {
////                    if (lambdasSequence.isEmpty()) {
////                        gs = gc.acceptMove(map, RobotMovement.ABORT);
////                        break;
////                    }
//		    Position currentRobotPosition = GameController.detectRobotPosition(map);
//		    Position nextLambda =null;
//		    do{
//			lambdasSequence = globalSearch.defineLambdasSequence(map, unreach_L_list);
//			if (lambdasSequence.size()==0 || lambdasSequence==null){
//			    //no more lambdas
//			    //we check if there is opened lift
//			    Position open_lift = MapUtils.searchOpenLift(map);
//			    if (open_lift!=null){
//				//we found opened lift!
//				lambdasSequence.add(open_lift);
//				break;
//			    }else{
//				gs = gc.acceptMove(map, RobotMovement.ABORT);
//				break;
//			    }
//			} else{
//			    nextLambda=lambdasSequence.get(0);
//			    localSearch.rebuildPath(map, currentRobotPosition, nextLambda);
//			    if(localSearch.ifValidWay()){
//				break;
//			    }else{
//				unreach_L_list.add(nextLambda);
//			    }
//			}
//		    }while(true);
//		    
//		    if (gs == GameStatus.IN_PROGRESS){
////			int div = 6;
////			for (int i = 0; i < lambdasSequence.size(); i++) {
////			if (i == 0) System.out.print(lambdasSequence.get(i));
////			else if (i % div == 0) {
////			    System.out.print("->");
////			    System.out.print("\n");
////			    System.out.print(" -> " + lambdasSequence.get(i));
////			} else System.out.print(" -> " + lambdasSequence.get(i));
////			}
//
//			//Position nextLambda = lambdasSequence.get(0);
//			System.out.println("Next lambda: " + nextLambda);
//			//currentRobotPosition = GameController.detectRobotPosition(map);
//			//localSearch.rebuildPath(map, currentRobotPosition, nextLambda);
//			one_step_wait=true;
//			do {
//			    System.out.println("\t Robot: " + currentRobotPosition);
//			    RobotMovement nextMovement = localSearch.defineNextRobotMovement(currentRobotPosition);
//			    if (nextMovement==null){
//				//then lyambra that we are trying to reach is unreachable. We must exclude it for global search
//				unreach_L_list.add(nextLambda);
//				break;
//			    } 
//			    //if we got danger or rock or wall on our way - rebuild lockal path
//			    if(!MapUtils.isMovementAvailable(map, currentRobotPosition, currentRobotPosition.apply(nextMovement))){
//				localSearch.rebuildPath(map, currentRobotPosition, nextLambda);
//				nextMovement = localSearch.defineNextRobotMovement(currentRobotPosition);
//				if (nextMovement==null){
//				//then lyambra that we are trying to reach is unreachable. We must exclude it for global search
//				unreach_L_list.add(nextLambda);
//				break;
//			    } 
//			    }
//			       
//
//			    System.out.println("\t Movement: " + nextMovement);
//			    if(nextMovement==null){
//				if(one_step_wait==true){
//				    one_step_wait=false;
//				    gs = gc.acceptMove(map, RobotMovement.WAIT);
//				    nextMovement=RobotMovement.WAIT;
//				}
//				else{
//				    gs = gc.acceptMove(map, RobotMovement.ABORT);
//				    autoMoves = autoMoves + nextMovement;
//				    break;
//				}
//			    }
//			    else{
//				gs = gc.acceptMove(map, nextMovement);
//				one_step_wait=true;
//			    }
//			    autoMoves = autoMoves + nextMovement;
//			    UpdateFlags flags = MapGenerator.updateMap(map, nextMovement);
//			    //System.out.println(map);
//			    currentRobotPosition = GameController.detectRobotPosition(map);
//			    if (gs != GameStatus.IN_PROGRESS) {
//				break;
//			    }
//			    if (flags.isUpdateLocalSearch()) {
//				localSearch.rebuildPath(map, currentRobotPosition, nextLambda);
//			    }
//			} while (!currentRobotPosition.equals(nextLambda));
//			if (gs != GameStatus.IN_PROGRESS) {
//			    break;
//			}
//		    }
//                }
//		System.out.println(map);
//		System.out.println("Lyambdas collected:" + gc.getLambdasCollected());
//                System.out.println("Result:" + gs);
//                System.out.println("Score:" + gc.getScore());
                
                frame.remove(autoPanel);
                autoPanel = new JPanel();
                autoPanel.setSize(31*mapAuto.getCols(), 31);
                autoPanel.setBackground(Color.white);
        
                autoPanel.add(new JLabel("Result:" + gs));
                autoPanel.add(new JLabel("Score:" + am.getScore()));
                
                
                autoShowButton.setMargin(new Insets(-3, -3, -3, -3));
                autoPanel.add(autoShowButton);
                
                
                frame.add(autoPanel);
                frame.revalidate();
                frame.repaint();
                
            }
        });
        
        
        autoShowButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                
                
                frame.remove(autoPanel);
                autoPanel = new JPanel();
                autoPanel.setSize(31*mapAuto.getCols(), 31);
                
                autoPanel.add(new JLabel("Score: 0"));
                
                frame.remove(panel);
                panel = new JPanel();
                panel.setLayout(new FlowLayout(FlowLayout.LEFT,1,1));
                panel.setBackground(Color.LIGHT_GRAY); 
                
                for(int x=0; x<mapAuto.getCols(); x++){
                    panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("free.png"))));
                }

                for(int y=mapAuto.getRows()-1; y>=0; y--){
                    for(int x=0; x<mapAuto.getCols(); x++){
                        if(mapAuto.at(y,x)==MineLayout.ROCK){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("bomb.png")))); }

                        else if(mapAuto.at(y,x)==MineLayout.LAMBDA){ 
                            panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("grape.png")))); 
                            lyambdas += 1;
                        }
                        else if(mapAuto.at(y,x)==MineLayout.EMPTY){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("free.png")))); }
                        else if(mapAuto.at(y,x)==MineLayout.LIFT_CLOSED){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("gates.png")))); }
                        else if(mapAuto.at(y,x)==MineLayout.LIFT_OPENED){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("gatesopen.png")))); }
                        else if(mapAuto.at(y,x)==MineLayout.SOIL){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("herb.png")))); }
                        else if(mapAuto.at(y,x)==MineLayout.ROBOT){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("robot.png")))); }
                        else if(mapAuto.at(y,x)==MineLayout.WALL){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("bricks.png")))); }
                        else { panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("free.png")))); }
                    }
                }

                frame.add(autoPanel);
                frame.add(panel);
                frame.setSize(31*mapAuto.getCols()+7, 31*mapAuto.getRows()+60);
                frame.revalidate();
                frame.repaint();
        
                new Main(0);
                
            }
        });
        
    }
    
    
    
    public void keyReleased(KeyEvent e) { //Key press checking
        
	RobotMovement rm=RobotMovement.WAIT;;
        if(e.getKeyCode()==37){ //Left button
            rm=RobotMovement.LEFT;
        } //Left button
        
        else if(e.getKeyCode()==38){ //Up button
            rm=RobotMovement.UP;
        } //Up button
        
        else if(e.getKeyCode()==39){ //Right button
            rm=RobotMovement.RIGHT;
        } //Right button
        
        else if(e.getKeyCode()==40){ //Down button
	    rm=RobotMovement.DOWN;
        } //Down button
        
        else if(e.getKeyCode()==17){ //Ctrl button
	    rm=RobotMovement.WAIT;
        } //Space button

        if(e.getKeyCode()==32){ //Space button
            gs = GameStatus.ABORT;
        } //Space button
	else{
	    Position robotP = GameController.detectRobotPosition(map);
            Position newRobotP = robotP.apply(rm);
            if(MapUtils.isMovementTheoreticallyAvailable(map, robotP, newRobotP)){
                gs = gc.acceptMove(map, rm);
                UpdateFlags flags = MapGenerator.updateMap(map, rm);
            }
	}
        
        frame.remove(panel);
        frame.remove(autoPanel);
        if(gs==GameStatus.IN_PROGRESS){

            panel = new JPanel();

            panel.setLayout(new FlowLayout(FlowLayout.LEFT,1,1));
            panel.setBackground(Color.LIGHT_GRAY); 
            
            for(int x=0; x<mapAuto.getCols(); x++){
                panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("free.png"))));
            }

            for(int y=map.getRows()-1; y>=0; y--){
                for(int x=0; x<map.getCols(); x++){
                    if(map.at(y,x)==MineLayout.ROCK){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("bomb.png")))); } 
                    else if(map.at(y,x)==MineLayout.LAMBDA){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("grape.png")))); }
                    else if(map.at(y,x)==MineLayout.EMPTY){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("free.png")))); }
                    else if(map.at(y,x)==MineLayout.LIFT_CLOSED){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("gates.png")))); }
                    else if(map.at(y,x)==MineLayout.LIFT_OPENED){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("gatesopen.png")))); }
                    else if(map.at(y,x)==MineLayout.SOIL){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("herb.png")))); }
                    else if(map.at(y,x)==MineLayout.ROBOT){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("robot.png")))); }
                    else if(map.at(y,x)==MineLayout.WALL){ panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("bricks.png")))); }
                    else { panel.add(new JLabel(new ImageIcon(Main.class.getClassLoader().getResource("free.png")))); }
                }
            }
            
            autoPanel = new JPanel();
                autoPanel.setSize(31*mapAuto.getCols(), 31);
                
                autoPanel.add(new JLabel("Score: "+gc.getScore()));
                
                frame.add(autoPanel);
            

            frame.add(panel);
            
            
            
        }else if(gs==GameStatus.WIN){
            finalPanel.add(new JLabel("������ :D "+gc.getScore()+" �����."));
            frame.remove(typingArea);
            frame.add(finalPanel);
        }else if(gs==GameStatus.LOOSE){
            finalPanel.add(new JLabel("���� :D "+gc.getScore()+" �����."));
            frame.remove(typingArea);
            frame.add(finalPanel);
        }else if(gs==GameStatus.ABORT){
            finalPanel.add(new JLabel("������ :D "+gc.getScore()+" �����."));
            frame.remove(typingArea);
            frame.add(finalPanel);
        }

        frame.revalidate();
        frame.repaint();

    }
    
    public void keyTyped(KeyEvent e) {}
    public void keyPressed(KeyEvent e) {}
    
}