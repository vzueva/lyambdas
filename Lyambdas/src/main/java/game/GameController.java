package game;

import entity.Map;
import entity.GameStatus;
import entity.MapExt;
import entity.MineLayout;
import entity.Position;
import entity.RobotMovement;
import map.utils.MapUtils;

/**
 * �����, ������������� ������� �������
 */
public final class GameController {


    private int score;
    private int lyambdas_collected;

    public GameController() {
        score = 0;
        lyambdas_collected = 0;
    }

    public int getScore() {
        return score;
    }
    public int getLambdasCollected() {
        return lyambdas_collected;
    }

    public static boolean validateRobotMovement(RobotMovement movement, Map map) {
        Position robotPosition = detectRobotPosition(map);
        MineLayout nextPos;
        // TODO
        return false;
    }
    /**
     * ������� ������������ ������� ������� ������
     * @param map ���������, ���������� ������� ��������� �����
     * @return ������� ������� ������ (��� robotPosition)
     */
    public static Position detectRobotPosition(Map map) {
        Position robotPosition = null;

        for (int i = 0; i < map.getRows(); i++) {
            for (int j = 0; j < map.getCols(); j++) {
                if (map.at(i, j) == MineLayout.ROBOT) {
                    robotPosition = new Position(i, j);
                    return robotPosition;
                }
            }
        }
        return robotPosition;
    }

    /**
     * �������, ��������������� ������� ������� ������. ��������� ��������
     * ���� ���� ����� �� �������� ��������
     * @param map - ������� ��������� �����
     * @param rm - ����������� ������� ��������
     * @return 
     */
    public static Position robotPosAfterMove(Map map, RobotMovement rm) {
        Position curr_pos = GameController.detectRobotPosition(map);
        assert (curr_pos != null);
        Position new_pos = curr_pos.apply(rm);
        if (MapUtils.isMovementTheoreticallyAvailable(map, curr_pos, new_pos)) {
            return new_pos;
        } else {
            return curr_pos;
        }
    }
    /**
     * �������, �����������, �������� �� �� ����� ��� ������
     * @param map - ������� ��������� �����
     * @return true, ���� �������� � false, ���� ���
     */
    public static boolean anyLambdasLeft(Map map) {
        for (int i = 0; i < map.getRows(); i++) {
            for (int j = 0; j < map.getCols(); j++) {
                if (map.at(i, j) == MineLayout.LAMBDA) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * �����, �����������, ��� �� ����� ����� ���������� ����
     * @param map - ������� ��������� �����
     * @param movement - ����������� ������� ��������
     * @return true ���� ����� ��� ��� � false, ���� ����
     */
    public static boolean ifAlive(MapExt map, RobotMovement movement) {
        if (map.isDanger(robotPosAfterMove(map, movement)))
            return false;
        else
            return true;
    }

    /**
     * �������, ����������� ����������� ���� � ��������������� ������� �����
     * � ��������� ����.
     * @param map ���������, ���������� ������� ��������� �����
     * @param rm ������� ��� ������
     * @return ������ ����
     */
    public GameStatus acceptMove(MapExt map, RobotMovement rm) {
        score--;
        if (rm == RobotMovement.ABORT) {
            score = score + lyambdas_collected * 25;
            return GameStatus.ABORT;
        }
        if (map.at(detectRobotPosition(map).apply(rm)) == MineLayout.LIFT_OPENED) {
            score = score + lyambdas_collected * 50;
            return GameStatus.WIN;
        }
        if (map.at(detectRobotPosition(map).apply(rm)) == MineLayout.LAMBDA) {
            //if robot get another lyambda
            score = score + 25;
            lyambdas_collected++;
        }
        if (ifAlive(map, rm)) {
            return GameStatus.IN_PROGRESS;
        } else {
	    score=0;
            return GameStatus.LOOSE;
        }
    }

    /**
     * �������, ������������ ���������� ���������� �� ����� �����
     * @param map ���������, ���������� ������� ��������� �����
     * @return ���������� ���������� �����
     */
    private int numLyambdasLeft(Map map) {
        int l = 0;
        for (int i = 0; i < map.getRows(); i++) {
            for (int j = 0; j < map.getCols(); j++) {
                if (map.at(i, j) == MineLayout.LAMBDA) {
                    l++;
                }
            }
        }
        return l;
    }
}
