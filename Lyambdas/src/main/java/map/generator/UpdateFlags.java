package map.generator;

/**
 * �����, ���������� �� ��������� ������ ����������� ���������� ���������� ������
 * @author Lyambdas Goup
 */
public class UpdateFlags {

    UpdateFlags() {
        updateGlobalSearch = false;
        updateLocalSearch = false;
    }

    UpdateFlags setUpdateLocalSearch() {
        updateLocalSearch = true;
        return this;
    }

    UpdateFlags setUpdateGlobalSearch() {
        updateGlobalSearch = true;
        return this;
    }
    /**
     * �������� ��������� ����� ���������� ������ ������� ����������� ������
     * @return ��������� �����
     */
    public boolean isUpdateGlobalSearch() {
        return updateGlobalSearch;
    }
    /**
     * �������� ��������� ����� ���������� ������ ������� ���������� ������
     * @return ��������� �����
     */
    public boolean isUpdateLocalSearch() {
        return updateLocalSearch;
    }

    private boolean updateGlobalSearch;
    private boolean updateLocalSearch;

}
