package map.generator;

import entity.*;
import game.GameController;
import map.utils.MapUtils;

import static entity.MineLayout.*;

/**
 * �����, ������������ ����� ��������� �����, ��������� �� �������� ��������� �����
 * � ��������� ������� �����
 * @author Lyambdas Group
 */
public class MapGenerator {
    /**
     * �������, ������������ ����� ��������� ����� � ����������� �� ���������� �����
     * �������. ��� �� ��������� ������� ������� ������ �� ��� �������� ��������
     * @param map ���������, ���������� ������� ��������� �����
     * @param robotMovement ������� �������� ������
     * @return ����� ����������� ������ ���������� ����������/����������� �������
     */
    public static UpdateFlags updateMap(MapExt map, RobotMovement robotMovement) {
        MineLayout[][] newMapCells = new MineLayout[map.getRows()][map.getCols()];

        map.clearDangers();
        UpdateFlags flags = new UpdateFlags();

        Position robotP = GameController.detectRobotPosition(map);
        Position newRobotP = robotP.apply(robotMovement);

	if (map.at(newRobotP).equals(LAMBDA)) {
            flags.setUpdateGlobalSearch();
        }
        //assert (MapUtils.isMovementAvailable(map, robotP, newRobotP));
	//System.out.println(MapUtils.isMovementAvailable(map, robotP, newRobotP));
	//robot make a move before map update:
	Position positionAfterDestination = newRobotP.nextPosition(robotP);
	if (MapUtils.isMovementTheoreticallyAvailable(map, robotP, newRobotP)){
	    //if robot moves rock
	    if(map.at(newRobotP)==ROCK){
		map.setMapCell(positionAfterDestination, ROCK);
		map.setMapCell(newRobotP, ROBOT);
		map.setMapCell(robotP, EMPTY);
		flags.setUpdateLocalSearch();
	    }
	    else{
		//if robot simple moves
		map.setMapCell(robotP, EMPTY);
		map.setMapCell(newRobotP, ROBOT);
	    }
	}
	
        //newMapCells[robotP.getRow()][robotP.getCol()] = EMPTY;
        //newMapCells[newRobotP.getRow()][newRobotP.getCol()] = ROBOT;
	boolean no_lambdas_left=true; //true, if there is no more lyambdas on new maps state 
        for (int i = 0; i < map.getRows(); i++) {
            for (int j = 0; j < map.getCols(); j++) {
                Position currentP = new Position(i, j);
		if (map.at(currentP)==MineLayout.LAMBDA){
		    no_lambdas_left=false;
		}
                Position downP = currentP.down();
                Position rightP = currentP.right();
                Position rightDownP = currentP.right().down();
                Position leftP = currentP.left();
                Position leftDownP = currentP.left().down();

//                if (currentP.equals(robotP) || currentP.equals(newRobotP)) {
//                    continue;
//                }

                //rock falls
                if (map.at(currentP) == ROCK && map.at(downP) == EMPTY) {
                    newMapCells[i][j] = EMPTY;
                    newMapCells[downP.getRow()][downP.getCol()] = ROCK;
                    //flags.setUpdateLocalSearch();
		    Position down_new_rock_pos=downP.down(); //just buffer, memory economy
		    if ((map.at(down_new_rock_pos.down())!=LIFT_OPENED) 
			&& MapUtils.isCellEmpty(map,down_new_rock_pos)) {
			map.addDanger(down_new_rock_pos.down());
		    }
                    continue;
                }

                //rock rolls right
                if (map.at(currentP) == ROCK && map.at(downP) == ROCK && map.at(rightP) == EMPTY && map.at(rightDownP) == EMPTY) {
                    newMapCells[i][j] = EMPTY;
                    newMapCells[rightDownP.getRow()][rightDownP.getCol()] = ROCK;
                    //flags.setUpdateLocalSearch();
		    Position rdd_pos=rightDownP.down(); //just buffer, memory economy
			if ((map.at(rdd_pos.down())!=LIFT_OPENED) 
			    &&(MapUtils.isCellEmpty(map,rdd_pos))){
			    map.addDanger(rdd_pos.down());
			}
                    continue;
                }
                //rock rolls left
                if (map.at(currentP) == ROCK && map.at(downP) == ROCK && map.at(leftP) == EMPTY && map.at(leftDownP) == EMPTY) {
                    newMapCells[i][j] = EMPTY;
                    newMapCells[leftDownP.getRow()][leftDownP.getCol()] = ROCK;
                    //flags.setUpdateLocalSearch();
		    Position ldd_pos=leftDownP.down(); //just buffer, memory economy
		    if ((map.at(ldd_pos.down())!=LIFT_OPENED) 
			    && (MapUtils.isCellEmpty(map, ldd_pos))){
			map.addDanger(ldd_pos.down());
		    }
                    continue;
                }
		
                //rock rolls right lyambda
                if (map.at(currentP) == ROCK && map.at(downP) == LAMBDA && map.at(rightP) == EMPTY && map.at(rightDownP) == EMPTY) {
                    newMapCells[i][j] = EMPTY;
                    newMapCells[rightDownP.getRow()][rightDownP.getCol()] = ROCK;
                    //flags.setUpdateLocalSearch();
		    Position rdd_pos=rightDownP.down(); //just buffer, memory economy
		    if ((map.at(rdd_pos.down())!=LIFT_OPENED) 
			    && (MapUtils.isCellEmpty(map, rdd_pos))){
			map.addDanger(rdd_pos.down());
		    }
                    continue;
                }

//		This doesn't work because we see map at its old state, but lift
//		must opened immediately when we eat last lambda
//                if (map.at(currentP) == LIFT_CLOSED && !GameController.anyLambdasLeft(map)) {
//                    newMapCells[i][j] = LIFT_OPENED;
//                    continue;
//                }

                if (map.at(currentP) == ROCK) {
		    //this made because robot mustn-t go down when it gots rock above
                    if (map.at(downP)==ROBOT){
			map.addDanger(downP.down());
			//flags.setUpdateLocalSearch();
		    }
		}
                newMapCells[i][j] = map.at(currentP);
            }
        }
	//if no more lyambdas - open lift
	if (no_lambdas_left==true){
	    for (int i = 0; i < map.getRows(); i++) 
		for (int j = 0; j < map.getCols(); j++) 
		    if (newMapCells[i][j]==MineLayout.LIFT_CLOSED){
			newMapCells[i][j]=MineLayout.LIFT_OPENED;
			break;
		    }
	}
        map.setMapContent(newMapCells);

        return flags;
    }

}
