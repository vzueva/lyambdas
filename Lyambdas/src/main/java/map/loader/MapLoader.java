package map.loader;

import entity.MineLayout;
import entity.Map;

import java.io.*;

/**
 * ����� ��� ������ ����� �� �����
 * ������������ ���������� ������ � ������� �������� ����
 * @author Lyambdas Group
 */
public final class MapLoader {

    private MapLoader() {
    }


    public static Map loadMapFromStream(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        MineLayout[][] mapContent;
        String mapString = "";
        int xs = 0;
        int ys = 0;

        while (bufferedReader.ready()) {
            mapString = bufferedReader.readLine();
            if (xs < mapString.length()) {
                xs = mapString.length();
            }
            ys++;
        }//������� ys=����� �����, xs=����� ������, mapString=��� ����� � ������

        mapContent = new MineLayout[ys][xs];
        for (int i = ys - 1; i >= 0; i--) {
            for (int j = 0; j < xs; j++) {
                mapContent[i][j] = MineLayout.EMPTY;
            }
        }
        if (stream.getClass().equals(FileInputStream.class)) {
            FileInputStream fileStream = (FileInputStream) stream;
            fileStream.getChannel().position(0);
        } else {
            stream.reset();
        }
        String s;
        for (int i = ys - 1; i >= 0; i--) {
            s = bufferedReader.readLine();
            for (int j = 0; j < s.length(); j++) {
                mapContent[i][j] = readMapSymbol(s.charAt(j));
            }
        }
        bufferedReader.close();

        return new Map(mapContent);
    }


    /**
     * ������ ����� �� �����
     * @param fileName ��� �������� ����� � ������
     * @return map ��������� �����
     * @throws IOException
     */
    public static Map loadMapFromFile(String fileName) throws IOException {
        InputStream fileStream = new FileInputStream(fileName);
        return loadMapFromStream(fileStream);
    }

    /**
     * ������� ������ ����� �� ������ ���� String
     * @param map ������, ���������� ��������� �����
     * @return map ������� ��������� �����
     * @throws IOException 
     */
    public static Map loadMapFromString(String map) throws IOException {
        InputStream stringStream = new ByteArrayInputStream(map.getBytes("UTF-8"));
        return loadMapFromStream(stringStream);
    }
    /**
     * �������, �������� � ������������ ����������� ������� ������������ �����,
     * ������ �������� ������������
     * @param symbol ������� ������ ��� �����������
     * @return ������ �������� ������������
     */
    private static MineLayout readMapSymbol(char symbol) {
        switch (symbol) {
            case '.':
                return MineLayout.SOIL;
            case '*':
                return MineLayout.ROCK;
            case 'R':
                return MineLayout.ROBOT;
            case 'L':
                return MineLayout.LIFT_CLOSED;
            case '#':
                return MineLayout.WALL;
            case '\\':
                return MineLayout.LAMBDA;
            case 'O':
                return MineLayout.LIFT_OPENED;
            default:
                return MineLayout.EMPTY;
        }
    }
}
