package map.utils;

import entity.Map;
import entity.MapExt;
import entity.MineLayout;
import entity.Position;

/**
 * 
 * @author Lyamdas Group
 */
public class MapUtils {

    /**
     * �������, ����������� ������� ������ � ��������� ������������
     * @param map ������, ���������� ������� ��������� �����
     * @param cellPos ������� ������ �� �����
     * @return true, ���� ������ �����, false, ���� ������ ������ �����-���� ��������
     */
    public static boolean isCellEmpty(Map map, Position cellPos) {
        MineLayout cell = map.at(cellPos);
        switch (cell) {
            case EMPTY:
            {
                return true;
            }
	    case ROBOT:
            case LAMBDA:
            case LIFT_OPENED:
            case SOIL:
            case ROCK:
            case WALL: {
                return false;
            }
        }
        return false;
    }
    /**
     * �������, ����������� ����������� ��� ���� �����-���� ������ �����
     * @param map ���������, ���������� ������� ��������� �����
     * @param cellPos �������, ����������� ������� ���������� ���������
     * @return true, ���� ������� ��������, false ���� ������� ����������
     */
      public static boolean isCellAvailable(Map map, Position cellPos) {
        MineLayout cell = map.at(cellPos);
        switch (cell) {
	    case ROBOT:
            case LAMBDA:
            case LIFT_OPENED:
            case SOIL:
            case EMPTY:
            {
                return true;
            }
	    case LIFT_CLOSED:
            case ROCK:
            case WALL: {
                return false;
            }
        }
        return false;
    }
    
    /**
     * �������, ����������� ������������� ����������� ���� � �������� �����������
     * @param map ���������, ���������� ������� ��������� �����
     * @param from ��������� �������� �����
     * @param to ���������� ������ ����������
     * @return true, ���� ��� ��������, false - ���� ���
     */
    public static boolean isMovementTheoreticallyAvailable(Map map, Position from, Position to) {
        MineLayout destinationCell = map.at(to);
        switch (destinationCell) {
            case LIFT_OPENED:
            case EMPTY:
            case LAMBDA:
            case SOIL:
            case ROBOT: {
                return true;
            }
            case ROCK: {
                Position positionAfterDestination = to.nextPosition(from);
                return (isCellEmpty(map, positionAfterDestination) && (to.getRow()<=from.getRow()));
            }
            case WALL: {
                return false;
            }
        }
        return false;
    }
    /**
     * �������, ����������� ������������ ����������� ����
     * @param map ���������, ���������� ������� ��������� �����
     * @param from ��������� �������� �����
     * @param to ���������� ������ ����������
     * @return true, ���� ��� ��������, false - ���� ���
     */
    public static boolean isMovementAvailable(MapExt map, Position from, Position to) {

        MineLayout destinationCell = map.at(to);
        switch (destinationCell) {
            case LIFT_OPENED: {
                return true;
            }

            case EMPTY:
            case LAMBDA:
	    case SOIL:{
                return !map.isDanger(to);
            }
            case ROBOT: {
                return true;
            }

            case ROCK: {
                //Position positionAfterDestination = to.nextPosition(from);
		//if rock moves left or right, NOT UP (to.getRow()<=from.getRow())!
               // return (isCellAvailable(map, positionAfterDestination) && (to.getRow()<=from.getRow()));
		return false;
            }

            case WALL: {
                return false;
            }
        }
        return false;
    }
    
    /**
     * Returns position of OPENED_LIFT ot null if there's no OPENED_LIFT
     */
    /**
     * �������, ������������ ������� ��������� �����
     * @param map ���������, ���������� ������� ��������� �����
     * @return ������� ��������� �����
     */
    public static Position searchOpenLift (Map map){
	for (int i=0; i<map.getRows(); i++) {
	    for (int j=0; j<map.getCols(); j++){
		if (map.at(i, j)==MineLayout.LIFT_OPENED) {
		    return new Position(i,j);
		}
	    }
	}
	return null;
    }


}
