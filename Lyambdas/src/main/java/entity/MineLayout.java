package entity;

/**
 * ����� ��� ������������� ���� ��������� �������� �� �����
 * @author Lyambdas Group
 * 
 */
public enum MineLayout {

 
    ROBOT {                         //��� - �����
        @Override
        public String toString() {
            return "R";
        }
    },

 
    ROCK {                          //��� - ������
        @Override
        public String toString() {
            return "*";
        }
    },


    LIFT_CLOSED {                   //���- �������� ����� � �����
        @Override
        public String toString() {
            return "L";
        }
    },


    LIFT_OPENED {                   //��� - �������� ����� � �����
        @Override
        public String toString() {
            return "O";
        }
    },


    SOIL {                          //��� - ???
        @Override
        public String toString() {
            return ".";
        }
    },

    WALL {                          //��� - �����
        @Override
        public String toString() {
            return "#";
        }
    },


    LAMBDA {                        //��� - ������
        @Override
        public String toString() {
            return "\\";
        }
    },


    EMPTY {                         //��� - ������ �������
        @Override
        public String toString() {
            return " ";
        }
    }
}
