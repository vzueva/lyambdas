package entity;

/**
 * 
 * @author Lyambdas Goup
 */
public class Position {
    private final int row;
    private final int col;
    /**
     * ���������� ����� �������
     * @param row - ������� ������
     * @param col - ������� �������
     */
    public Position(int row, int col) {
        this.row = row;
        this.col = col;
    }
    /**
     * ������� �������� ������� �������
     * @return - ������� ������
     */
    public int getRow() {
        return row;
    }
    /**
     * ������� ��������� ������� �������
     * @return - ������� �������
     */
    public int getCol() {
        return col;
    }

    @Override
    /**
     * �������� �� ������������� ��������, � ���������, ������� � ������� �������
     * @return true ���� ������� ����������, false - ���� ���
     */
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        if (col != position.col) return false;
        if (row != position.row) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = row;
        result = 31 * result + col;
        return result;
    }

    @Override
    /**
     * ������� ����������� ������� ������� � ��������� ����
     * @return ������, � ������� ������� ������� �������
     */
    public String toString() {
        return "(" + row + ", " + col + ")";
    }
    /**
     * ������� ���������� �������� ������
     * @param rows - �������� �� ��� Y (������)
     * @param cols - �������� �� ��� X (�������)
     * @return - ����� ������� ������, � ������ ��������
     */
    public Position offset(int rows, int cols) {
        return new Position(this.row + rows, this.col + cols);
    }
    /**
     * �������� ������� "����������� �� ��� �������� �����"
     * @return �������� �� ��� ��������� ����� (��� Y)
     */
    public Position up() {
        return offset(1, 0);
    }
    /**
     * �������� ������� "����������� �� ��� �������� �����"
     * @return �������� �� ��� ��������� ����� (��� X)
     */
    public Position left() {
        return offset(0, -1);
    }
    /**
     * �������� ������� "����������� �� ��� �������� ������"
     * @return �������� �� ��� ��������� ������ (��� X)
     */
    public Position right() {
        return offset(0, 1);
    }
    /**
     * �������� ������� "����������� �� ��� �������� ����"
     * @return �������� �� ��� ��������� ���� (��� Y)
     */
    public Position down() {
        return offset(-1, 0);
    }
    /**
     * ���������� �������� �������� ������ �� ���� �����
     * @param movement - ������� ��� ���������� ��������
     * @return - �������� �� ��� ���������
     */
    public Position apply(RobotMovement movement) {
        switch (movement) {
            case LEFT:
                return this.left();
            case RIGHT:
                return this.right();
            case UP:
                return this.up();
            case DOWN:
                return this.down();
            case WAIT:
                return this.offset(0, 0);
        }
        assert (false);
        return this.offset(0, 0);
    }

    /**
     * ������� ���������� ������� ��������� �� ���, � ������� ������ �����. ���������� ��� ���������� �����������
     * ������������ ������.
     * @param from ��������� �������, ������ �������� ���
     * @return ��������� ������� �� ���� ��������
     */
    public Position nextPosition(Position from) {
        return new Position(this.row + this.row - from.row, this.col + this.col - from.col);
    }
    /**
     * ���������� ��������� ����� ����� ���������
     * @param to - Position(int row, int col) - ���������� ����
     * @return - ���������� �� ������� ������� � ������� �������� ����
     */
    public int distanceTo(Position to) {
        return Math.abs(this.col - to.col) + Math.abs(this.row - to.row);
    }

}
