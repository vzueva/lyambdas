package entity;

import entity.MineLayout;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ����� �� ������������� �����
 * ������������ ����� ��������� ������ ���� MineLayout
 * @author Lyambdas Group
 */
public class Map {

    protected MineLayout[][] map;
    protected int cols;
    protected int rows;


    /**
     * ����������� ��� �������� ����� �������� ����� � ���������� �� � ������
     * @param rows - ���������� ������� � �����
     * @param cols - ���������� ������� � �����
     */
    public Map(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        map = new MineLayout[rows][cols];
    }
    /**
     * 
     * @param mapContent 
     */

    public Map(MineLayout[][] mapContent) {
        this.rows = mapContent.length;
        this.cols = mapContent[0].length;
        map = mapContent;
    }
    /**
     * 
     * @param map 
     */

    public Map(Map map) {
        this.rows = map.rows;
        this.cols = map.cols;
        this.map = map.map;
    }
    /**
     * 
     * @param mapContent 
     */
    public void setMapContent(MineLayout[][] mapContent) {
        this.rows = mapContent.length;
        this.cols = mapContent[0].length;
        map = mapContent;
    }

    /**
     * ������� ��������� ���������� ������� �����
     * @return ���������� �������
     */
    public int getRows() {
        return rows;
    }

    /**
     * ������� ��������� ���������� ������� �����
     * @return ���������� �������
     */
    public int getCols() {
        return cols;
    }


    /**
     * 
     * @param row
     * @param col
     * @return
     */
    public MineLayout at(int row, int col) {
	if (row>=0 && col >=0){
	    return map[row][col];
	}
	else
	    return null;
    }

    /**
     *
     * @param row
     * @param col
     * @return
     */
    public MineLayout top(int row, int col) {
        if (row == rows - 1) {
            return null;
        }
        return map[row + 1][col];
    }

    /**
     *
     * @param row
     * @param col
     * @return
     */
    public MineLayout left(int row, int col) {
        if (col == 0) {
            return null;
        }
        return map[row][col - 1];
    }

    /**
     *
     * @param row
     * @param col
     * @return
     */
    public MineLayout right(int row, int col) {
        if (col == cols - 1) {
            return null;
        }
        return map[row][col + 1];
    }

    /**
     *
     * @param row
     * @param col
     * @return
     */
    public MineLayout bottom(int row, int col) {
        if (row == 0) {
            return null;
        }
        return map[row - 1][col];
    }


    /**
     * ���� ������� ������� ����������� ����. ���������� ������� ������� �������������� ��� ������ �������.
     * ���� ������ ������� ���, ���������� null
     * @param obj ��� ������� �������� ������������ ��� ������
     * @return ������� ���������� �������, null -  ���� ������� ���
     */
    public Position firstObjsEntrySearch (MineLayout obj){
	for (int i=0; i<getRows(); i++){
	    for (int j=0; j<getCols(); j++){
		if (this.at(i, j)==obj){
		    return new Position(i, j);
		}
	    }
	}
	return null;
    }

    /**
     *
     * @param pos
     * @return
     */
    public MineLayout at(Position pos) {
        return at(pos.getRow(), pos.getCol());
    }

    /**
     *
     * @param pos
     * @return
     */
    public MineLayout top(Position pos) {
        return top(pos.getRow(), pos.getCol());
    }

    /**
     *
     * @param pos
     * @return
     */
    public MineLayout left(Position pos) {
        return left(pos.getRow(), pos.getCol());
    }

    /**
     *
     * @param pos
     * @return
     */
    public MineLayout right(Position pos) {
        return right(pos.getRow(), pos.getCol());
    }

    /**
     *
     * @param pos
     * @return
     */
    public MineLayout bottom(Position pos) {
        return bottom(pos.getRow(), pos.getCol());
    }

    ///////////////////////////////////////////////
    
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = rows - 1; i >= 0; i--) {
            for (int j = 0; j < cols; j++) {
                result.append(this.map[i][j] + " ");
            }
            result.append("\n");
        }
        return result.toString();
    }
}
