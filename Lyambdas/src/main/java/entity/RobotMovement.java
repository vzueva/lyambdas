package entity;

/**
 * ����� ��� ������������� ��������� �������� ������
 */
public enum RobotMovement {
    //����������� �����

    LEFT {
        @Override
        public String toString() {
            return "L";
        }
    },
    //����������� ������

    RIGHT {
        @Override
        public String toString() {
            return "R";
        }
    },
    //����������� �����
 
    UP {
        @Override
        public String toString() {
            return "U";
        }
    },
    //����������� ����

    DOWN {
        @Override
        public String toString() {
            return "D";
        }
    },
    //�������� (������� ����)

    WAIT {
        @Override
        public String toString() {
            return "W";
        }
    },
    //������ ����

    ABORT {
        @Override
        public String toString() {
            return "A";
        }
    }
}
