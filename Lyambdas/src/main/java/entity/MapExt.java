package entity;


import java.util.ArrayList;
import java.util.LinkedList;

import entity.MineLayout;
import entity.RobotMovement;

import java.util.List;
import java.util.Set;

public class MapExt extends Map {

    private List<Position> dangers = new ArrayList<Position>();

    public MapExt(int rows, int cols) {
        super(rows, cols);
    }

    public MapExt(MineLayout[][] mapContent) {
        super(mapContent);
    }

    public MapExt(Map map) {
        super(map);
    }

    public boolean isDanger(int row, int col) {
        return dangers.indexOf(new Position(row, col)) != -1;
    }

    public boolean isDanger(Position pos) {
        return dangers.indexOf(pos) != -1;
    }

    public void clearDangers() {
        dangers.clear();
    }

    public void addDanger(Position pos) {
        dangers.add(pos);
    }
/*
    public Position findClosedLift(){
	for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getCols(); j++) {
		if (this.at(i, j) == MineLayout.LIFT_CLOSED) {
		    return new Position(i, j);
		}
	    }
	}
	return null;
    }
    */
    public void setMapCell(Position p, MineLayout content){
	map[p.getRow()][p.getCol()]=content;
    }
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = rows - 1; i >= 0; i--) {
            result.append(i + " ");
            for (int j = 0; j < cols; j++) {
                if (isDanger(i, j)) {
                    result.append("(" + this.map[i][j] + ") ");
                } else {
                    result.append(" " + this.map[i][j] + "  ");
                }
            }
            result.append("\n");
        }
        result.append("  ");
        for (int j = 0; j < cols; ++j) {
            result.append(String.format("%2d", j) + "  ");
        }
        return result.toString();
    }
}
