/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.art.intelligence.global;

import entity.Map;
import entity.MineLayout;
import entity.Position;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;
import map.utils.MapUtils;
/**
 *
 * @author HOME
 */
public class FloodfillGlobalSearch implements art.intelligence.global.GlobalSearch{
    private List<Position> close_list;
    private List<Position> opened_list;
    
    public FloodfillGlobalSearch(){
	close_list=new LinkedList<Position>();
	opened_list=new LinkedList<Position>();
    }
    
    @Override
    public List<Position> defineLambdasSequence(Map map, List<Position> exclude_l_l) {
	close_list.clear();
	opened_list.clear();
	Position rob_pos = map.firstObjsEntrySearch(MineLayout.ROBOT);
	opened_list.add(rob_pos);
	Position lambda_pos=null;
	assert(rob_pos!=null);
	while (!opened_list.isEmpty()) {
	     Position curr_opened_pos=opened_list.iterator().next();
	     if(map.at(curr_opened_pos)==MineLayout.LAMBDA || map.at(curr_opened_pos)==MineLayout.LIFT_OPENED){
		 //meet lambda at open position 
		 //must check that this is not a lambda from exclude_l_l
		 if(exclude_l_l.contains(curr_opened_pos)){
		     close_list.add(curr_opened_pos);
		     opened_list.remove(curr_opened_pos);
		     continue;
		 }
		 //YES! We'v found next lambda!
		 lambda_pos=curr_opened_pos;
		 break;
	     }
	     else{
		 for (Position y : neighbor_nodes(curr_opened_pos, map)) {
		     if (!close_list.contains(y) && !opened_list.contains(y)) {
			// ��������� ������� ������� ��� �� � �������� �� � �������� �������
			opened_list.add(y);
		    }
		 }
		 close_list.add(curr_opened_pos);
		 opened_list.remove(curr_opened_pos);
	     }
	 }
	    LinkedList<Position> ll = new LinkedList<Position>();
	    if(lambda_pos!=null){
		ll.add(lambda_pos);
	    }
	    return ll;
    }
    
    private List<Position> neighbor_nodes(Position node, Map map) {
        List<Position> result = new ArrayList<Position>(4);
	{
            Position nodeDownPos = node.down();
            if (MapUtils.isCellAvailable(map, nodeDownPos)) {
                result.add(nodeDownPos);
            }
        }
        {
            Position nodeTopPos = node.up();
            if (MapUtils.isCellAvailable(map, nodeTopPos)) {
                result.add(nodeTopPos);
            }
        }
	{
            Position nodeRightPos = node.right();
            if (MapUtils.isCellAvailable(map, nodeRightPos)) {
                result.add(nodeRightPos);
            }
        }
	{
            Position nodeLeftPos = node.left();
            if (MapUtils.isCellAvailable(map, nodeLeftPos)) {
                result.add(nodeLeftPos);
            }
        }
        return result;
    }
    
}
