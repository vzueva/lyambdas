package art.intelligence.global;


import entity.Map;
import entity.MineLayout;
import entity.Position;

import java.util.LinkedList;
import java.util.List;


public class CommiGlobalSearch implements GlobalSearch {

    public List<Position> defineLambdasSequence(Map map, List<Position> exclude_lys) {

        List<Position> lambdasPositions = new LinkedList<Position>();

        int r1 = 0, c1 = 0, r2 = 0, c2 = 0;
        for (int i = 0; i < map.getRows(); i++) {
            for (int j = 0; j < map.getCols(); j++) {
                if (map.at(i, j) == MineLayout.LAMBDA) {
		    Position lp = new Position(i, j);
		    if(!exclude_lys.contains(lp))
			lambdasPositions.add(new Position(i, j));
                } else if (map.at(i, j) == MineLayout.ROBOT) {
                    lambdasPositions.add(0, new Position(i, j));
                }
            }
        }
        if (lambdasPositions.size() == 1) {
            lambdasPositions.remove(0);
            return lambdasPositions;
        }
        int n = lambdasPositions.size();
        int arr[][] = new int[n][n];

        for (int i = 0; i <= lambdasPositions.size() - 1; i++) {
            for (int j = 0; j <= lambdasPositions.size() - 1; j++) {
                if (i == j) {
                    arr[i][j] = 0;
                } else if (j > i) {
                    r1 = lambdasPositions.get(i).getRow();
                    c1 = lambdasPositions.get(i).getCol();
                    r2 = lambdasPositions.get(j).getRow();
                    c2 = lambdasPositions.get(j).getCol();
                    r1 = Math.abs(r1 - r2);
                    c1 = Math.abs(c1 - c2);

                    arr[i][j] = r1 + c1;
                    arr[j][i] = r1 + c1;
                }
            }
        }
        lambdasPositions.remove(0);

        return salesmanAlgoritm(arr, lambdasPositions);
    }

    public static int[][] droppingColumn(int[][] arr, int row, int length) {
        for (int i = 0; i < length; i++) {
            arr[i][row] = 0;
        }
        return arr;
    }

    public static List<Position> salesmanAlgoritm(int[][] arr, List<Position> lambdasPositions) {

        int cnt = 0;
        int min = 0;
        int div = arr.length;
        int s = 0;

        int n = lambdasPositions.size();
        int numLambdas[] = new int[n];
        numLambdas[0] = cnt;
        List<Position> lambdasPosAfter = new LinkedList<Position>();

        while (cnt < arr.length && s <= arr.length - 1) {

            arr = droppingColumn(arr, cnt, arr.length);

            if (cnt != 0 && s != 0) numLambdas[s - 1] = cnt;

            for (int i = 0; i < arr.length; i++) {
                if (arr[cnt][i] > 0) {
                    min = arr[cnt][i];
                    break;
                }
            }
            int cnt1 = cnt;

            for (int j = 0; j < arr.length; j++) {
                if (min >= arr[cnt1][j] && arr[cnt1][j] != 0) {
                    min = arr[cnt1][j];
                    cnt = j;
                }
            }
            s++;
        }


        for (int i = 0; i <= lambdasPositions.size() - 1; i++) {
            lambdasPosAfter.add(i, lambdasPositions.get(numLambdas[i] - 1));
        }

        return lambdasPosAfter;
    }

}

