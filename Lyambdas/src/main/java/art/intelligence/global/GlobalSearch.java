package art.intelligence.global;

import entity.Map;
import entity.Position;

import java.util.List;

/**
 * ����� ��� ������������� ��������� ����������� ������
 * ������� �������������� - ����������� ������������������ ����� ��� ������
 */
public interface GlobalSearch {

    /**
     * ���������� ������������������ ����� ��� ������
     *
     * @param map ���������, ���������� ������� ��������� �����
     * @param List exclude_l_l ������ ����������� �����
     * @return sequent list of lambdas ������ ���� �����
     */
    List<Position> defineLambdasSequence(Map map, List<Position> exclude_l_l );
}
