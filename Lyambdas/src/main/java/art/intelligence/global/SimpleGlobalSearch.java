package art.intelligence.global;

import entity.Map;
import entity.MineLayout;
import entity.Position;

import java.util.LinkedList;
import java.util.List;

/**
 * �����, �������������� �������� ��������� ����������� ������
 * ����� �������� ������� ��� ������ � ��� �������, � ������� ��� ����������� �� �����
 * @author Lyambdas Group
 */
public final class SimpleGlobalSearch implements GlobalSearch {

    public List<Position> defineLambdasSequence(Map map, List<Position> exclude_l_l) {
        List<Position> lambdasPositions = new LinkedList<Position>();

        for (int i = 0; i < map.getRows(); i++) {
            for (int j = 0; j < map.getCols(); j++) {
                if (map.at(i, j) == MineLayout.LAMBDA) {
                    lambdasPositions.add(new Position(i, j));
                }
            }
        }

        return lambdasPositions;
    }
}
