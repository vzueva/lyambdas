package art.intelligence.local;

import entity.*;
import entity.Map;
import map.utils.MapUtils;

import java.util.*;

public class AStarLocalSearch implements LocalSearch {

    static private class AStarNode {

        public AStarNode(Position position) {
            g = h = 0;
            came_from = null;
            pos = position;
        }

        public AStarNode came_from;
        public int g;
        public int h;

        public int f() {
            return g + h;
        }

        public final Position pos;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            AStarNode astarnode = (AStarNode) o;

            if (!pos.equals(astarnode.pos)) return false;

            return true;
        }
    }

    @Override
    public RobotMovement defineNextRobotMovement(Position currentRobotPosition) {
        assert (mPath != null);
	if (mPath.size()==0){
	    return null;
	}
        assert (mPath.size() > 0);
        RobotMovement result = mPath.get(0);
        mPath.remove(0);
        return result;
    }
    
    public boolean ifValidWay(){
	if (mPath.size()==0){
	    return false;
	}
	return true;
    }

    @Override
    public void rebuildPath(MapExt map, Position currentRobotPosition, Position nextLambdaPosition) {
        AStarNode startNode = new AStarNode(currentRobotPosition);
        AStarNode goalNode = new AStarNode(nextLambdaPosition);
        mMap = map;

        mPath = a_star(startNode, goalNode);
    }


    private final List<AStarNode> mClosedSet = new ArrayList<AStarNode>();
    private final List<AStarNode> mOpenSet = new ArrayList<AStarNode>();
    private MapExt mMap;
    private List<RobotMovement> mPath;

    private int heuristic_cost_estimate(AStarNode node1, AStarNode node2) {
        return node1.pos.distanceTo(node2.pos);
    }

    private List<AStarNode> neighbor_nodes(AStarNode node) {
        List<AStarNode> result = new ArrayList<AStarNode>(4);

	{
            Position nodeBottomPos = node.pos.down();
            if (MapUtils.isMovementAvailable(mMap, node.pos, nodeBottomPos)) {
                result.add(new AStarNode(nodeBottomPos));
            }
        }
        {
            Position nodeTopPos = node.pos.up();
            if (MapUtils.isMovementAvailable(mMap, node.pos, nodeTopPos)) {
                result.add(new AStarNode(nodeTopPos));
            }
        }
        {
            Position nodeLeftPos = node.pos.left();
            if (MapUtils.isMovementAvailable(mMap, node.pos, nodeLeftPos)) {
                result.add(new AStarNode(nodeLeftPos));
            }
        }
        {
            Position nodeRightPos = node.pos.right();
            if (MapUtils.isMovementAvailable(mMap, node.pos, nodeRightPos)) {
                result.add(new AStarNode(nodeRightPos));
            }
        }

        return result;
    }

    private int dist_between(AStarNode node1, AStarNode node2) {
        return 1;
    }

    private List<RobotMovement> a_star(AStarNode start, AStarNode goal) {

        mClosedSet.clear(); // ��������� ������, ������� ��� ���� ����������(��������)
        mOpenSet.clear();
        mOpenSet.add(start); // ��������� ������(�������), ������� ��������� ����������(��������).

        //��������� �������� ������� start
        start.g = 0;   // g(x). ��������� ���� �� ��������� �������. � start g(x) = 0
        start.h = heuristic_cost_estimate(start, goal); // ������������� ������ ���������� �� ����. h(x)

        while (!mOpenSet.isEmpty()) {

            AStarNode x = mOpenSet.iterator().next();//������� �� openset ������� ����� ������ ������ f(x)
            if (x.equals(goal)) {
                return reconstruct_path(x); //��������� ����� path_map
            }

            mOpenSet.remove(x);//remove x from openset // ������� x ����� �� ���������, � ������ � ������� ������� �� ������� �� ���������
            mClosedSet.add(x);//add x to closedset    // � �������� � ������ ��� ������������

            for (AStarNode y : neighbor_nodes(x)) {

                if (mClosedSet.contains(y)) {
                    // ���������� ������� �� ��������� ������
                    continue;
                }

                int tentative_g_score = x.g + dist_between(x, y);  // ��������� g(x) ��� ��������������� ������
                boolean tentative_is_better = false;

                if (!mOpenSet.contains(y)) {
                    // ���� ����� x ��� �� � �������� ������ - ������� ��� ����
                    mOpenSet.add(y);
                    tentative_is_better = true;
                } else {
                    // ����� ��� � �������� ������, � ������ �� ��� ����� ��� g(x), h(x) � f(x)
                    if (tentative_g_score < y.g) {
                        // ����������� g(x) ��������� ������, � ������ ����� ����� ��������  g(x), h(x), f(x)
                        tentative_is_better = true;
                    } else {
                        // ����������� g(x) ��������� ������, ��� ��������� � openset.
                        // ��� ��������, ��� �� ������� x ���� ����� ����� ������ ������
                        // �.�. ���������� ����� ������� �������, ����������� ����� ����� ������ (�� �����-�� ������ �������, �� �� x)
                        // ������� ������� ������ �� ����������
                        tentative_is_better = false;
                    }
                }
                // ���������� ������� ������.
                if (tentative_is_better) {
                    y.came_from = x; //������� � ������� �� ������. ������������ ��� ������������� ����.
                    y.g = tentative_g_score;
                    y.h = heuristic_cost_estimate(y, goal);
                    // �������� ��������, ��� ���� ���������� ���������� ������� - ������ y(����� x)
                    // ��� ��� ����� ��������� � openset.
                    // �.�. ��� ��������� �������� �������� ����� �� openset ����� ��������� ������� � ���������� ������� f(x)
                    // �� ���������, ��� ��� �������� ������� ������ x, �������� �� ������ ��� ��������.
                    // � ����� ��� ����� ������ ����������� ��������� �*
                }
            }
        }

        return new LinkedList<RobotMovement>();//���������� ��������� ���� ����� openset ����, � goal �� ������ (���� ����� �� �������)
    }

    private RobotMovement getMovement(AStarNode from, AStarNode to) {
        int yDiff = to.pos.getRow() - from.pos.getRow();
        int xDiff = to.pos.getCol() - from.pos.getCol();
        if (yDiff != 0) {
            return yDiff > 0 ? RobotMovement.UP : RobotMovement.DOWN;
        }
        return xDiff > 0 ? RobotMovement.RIGHT : RobotMovement.LEFT;
    }

    // ��������� ����� path_map
    // ���� ����� ���������� ������ �� �������� �������(���� ����� ��� goal)
    // � ������(������ ������� ����� �������� came_from, ��� �� � �������������)
    private List<RobotMovement> reconstruct_path(AStarNode goal_node) {

        List<RobotMovement> result = new LinkedList<RobotMovement>();

        // ��������� � ����� ��� ������� �� finish_node �� start_node.
        AStarNode current_node = goal_node; // ����� ���������� �� ������
        while (current_node.came_from != null) {
            result.add(0, getMovement(current_node.came_from, current_node)); // �������� ������� � �����
            current_node = current_node.came_from;
        }

        return result;
    }
}
