package art.intelligence.local;

import entity.Map;
import entity.MapExt;
import entity.Position;
import entity.RobotMovement;

/**
 * ����� ��� ������������� �������� ���������� ������.
 * �������� ���������� - ����� ���� �� ��������� ������
 * @author Lyambdas Group
 */
public interface LocalSearch {

    /**
     * ���������� ����������� ������
     *
     * @param currentRobotPosition  ������� ������� ������
     * @return RobotMovement ��������, ������� ������ ������� �����
     */
    RobotMovement defineNextRobotMovement(Position currentRobotPosition);
    boolean ifValidWay();
	 /**
     * ������� ��� ������ ���������� ����� ������ ����������� ������ �� ����
     * @param map ������� ��������� �����
     * @param currentRobotPosition ������� ������� ������
     * @param nextLambdaPosition ������� ��������� ������, � ������� ���������� ������
     */
    void rebuildPath(MapExt map, Position currentRobotPosition, Position nextLambdaPosition);
}
