package art.intelligence.local;

import entity.Map;
import entity.MapExt;
import entity.Position;
import entity.RobotMovement;

/**
 * �����, ������� ������������ ����� ���������� ��������� ����� ���� � ��������� ������.
 * @author Lyambdas Group
 */
public class SimpleLocalSearch implements LocalSearch {

    public RobotMovement defineNextRobotMovement(Position currentRobotPosition) {
        int yDifference = currentRobotPosition.getRow() - nextLambdaPosition.getRow();
        int xDifference = currentRobotPosition.getCol() - nextLambdaPosition.getCol();

        // ��������, ��������� �� �� �� ��� �� �������, ��� � ������� ������ �� ���������� �
        if (yDifference != 0) {
            if (yDifference > 0) {
                return RobotMovement.DOWN;
            } else {
                return RobotMovement.UP;
            }
        }
        // ��������, ��������� �� �� �� ��� �� �������, ��� � ������� ������ �� ���������� �
        if (xDifference != 0) {
            if (xDifference > 0) {
                return RobotMovement.LEFT;
            } else {
                return RobotMovement.RIGHT;
            }
        }

        return RobotMovement.WAIT;
    }

    @Override
    public void rebuildPath(MapExt map, Position currentRobotPosition, Position nextLambdaPosition) {
        this.nextLambdaPosition = nextLambdaPosition;
        this.map = map;
    }
    public boolean ifValidWay(){
	return true;
    }

    private Position nextLambdaPosition;
    private MapExt map;

}
