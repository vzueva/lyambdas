/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.automover;

import art.intelligence.global.CommiGlobalSearch;
import art.intelligence.global.GlobalSearch;
import art.intelligence.local.AStarLocalSearch;
import art.intelligence.local.LocalSearch;
import entity.GameStatus;
import entity.MapExt;
import entity.MineLayout;
import entity.Position;
import entity.RobotMovement;
import game.GameController;
import java.util.LinkedList;
import java.util.List;
import main.java.art.intelligence.global.FloodfillGlobalSearch;
import map.generator.MapGenerator;
import map.generator.UpdateFlags;
import map.utils.MapUtils;

/**
 *
 * @author HOME
 */
public class AutoMover {
    private MapExt map;
    private String moves_sequence="";
    private GameController gc; 
    
    public int getScore(){
	return gc.getScore();
    }
    
    public String getWay(){
	return moves_sequence;
    }    
    public AutoMover(MapExt map){
	this.map=map;
	gc = new GameController();
    }
    public GameStatus startAIWayBuilder (){
	System.out.println(map);
	System.out.println("==========================================");

	List<Position> unreach_L_list = new LinkedList<Position>(); //List with positions of unreachable lyambdas
	GlobalSearch globalSearch = new FloodfillGlobalSearch();
	LocalSearch localSearch = new AStarLocalSearch();
	List<Position> lambdasSequence = null;
	GameStatus gs = GameStatus.IN_PROGRESS;
	boolean one_step_wait=true;
	while (gs == GameStatus.IN_PROGRESS) {
	    Position currentRobotPosition = GameController.detectRobotPosition(map);
	    Position nextLambda =null;
	    do{
		lambdasSequence = globalSearch.defineLambdasSequence(map, unreach_L_list);
		if (lambdasSequence==null || lambdasSequence.size()==0){
		    //no more lambdas
		    //we check if there is opened lift
//		    Position open_lift = MapUtils.searchOpenLift(map);
//		    if (open_lift!=null){
//			//we found opened lift!
//			lambdasSequence.add(open_lift);
//			break;
//		    }else{
			gs = gc.acceptMove(map, RobotMovement.ABORT);
			break;
//		    }
		} else{
		    nextLambda=lambdasSequence.get(0);
		    localSearch.rebuildPath(map, currentRobotPosition, nextLambda);
		    if(localSearch.ifValidWay()){
			break;
		    }else{
			unreach_L_list.add(nextLambda);
		    }
		}
	    }while(true);

	    if (gs == GameStatus.IN_PROGRESS){
//			int div = 6;
//			for (int i = 0; i < lambdasSequence.size(); i++) {
//			if (i == 0) System.out.print(lambdasSequence.get(i));
//			else if (i % div == 0) {
//			    System.out.print("->");
//			    System.out.print("\n");
//			    System.out.print(" -> " + lambdasSequence.get(i));
//			} else System.out.print(" -> " + lambdasSequence.get(i));
//			}

		System.out.println("Next lambda: " + nextLambda);
		one_step_wait=true;
		do {
		    //System.out.println("\t Robot: " + currentRobotPosition);
		    RobotMovement nextMovement = localSearch.defineNextRobotMovement(currentRobotPosition);
		    if (nextMovement==null){
			//then lyambra that we are trying to reach is unreachable. We must exclude it for global search
			unreach_L_list.add(nextLambda);
			break;
		    } 
		    //if we got danger or rock or wall on our way - rebuild lockal path
		    if(!MapUtils.isMovementAvailable(map, currentRobotPosition, currentRobotPosition.apply(nextMovement))){
			localSearch.rebuildPath(map, currentRobotPosition, nextLambda);
			nextMovement = localSearch.defineNextRobotMovement(currentRobotPosition);
			if (nextMovement==null){
			//then lyambra that we are trying to reach is unreachable. We must exclude it for global search
			unreach_L_list.add(nextLambda);
			break;
		    } 
		    }

		    //System.out.println("\t Movement: " + nextMovement);
//		    if(nextMovement==null){
//			if(one_step_wait==true){
//			    one_step_wait=false;
//			    gs = gc.acceptMove(map, RobotMovement.WAIT);
//			    nextMovement=RobotMovement.WAIT;
//			}
//			else{
//			    gs = gc.acceptMove(map, RobotMovement.ABORT);
//			    autoMoves = autoMoves + nextMovement;
//			    break;
//			}
//		    }
//		    else{
			//if we eat lambdo on our way to another lambda, we must make sure, that this lambda wasn't at unavalabe lambdas list
			if (map.at(currentRobotPosition.apply(nextMovement))==MineLayout.LAMBDA){
			    unreach_L_list.remove(currentRobotPosition.apply(nextMovement));
			}
			gs = gc.acceptMove(map, nextMovement);
			one_step_wait=true;
//		    }
		    moves_sequence = moves_sequence + nextMovement;
		    UpdateFlags flags = MapGenerator.updateMap(map, nextMovement);
		    System.out.println(map);
		    currentRobotPosition = GameController.detectRobotPosition(map);
		    if (gs != GameStatus.IN_PROGRESS) {
			break;
		    }
		    if (flags.isUpdateLocalSearch()) {
			localSearch.rebuildPath(map, currentRobotPosition, nextLambda);
		    }
		} while (!currentRobotPosition.equals(nextLambda));
		if (gs != GameStatus.IN_PROGRESS) {
		    break;
		}
	    }
	}
	System.out.println(map);
	System.out.println("Lyambdas collected:" + gc.getLambdasCollected());
	System.out.println("Result:" + gs);
	System.out.println("Score:" + gc.getScore());
	return gs;
    }
}
