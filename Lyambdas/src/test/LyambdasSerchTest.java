package test;

import art.intelligence.global.GlobalSearch;
import art.intelligence.global.SimpleGlobalSearch;
import art.intelligence.local.AStarLocalSearch;
import art.intelligence.local.LocalSearch;
import entity.Map;
import entity.MapExt;
import entity.Position;
import map.loader.MapLoader;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class LyambdasSerchTest {

    static final String map1 =
            "######\n" +
            "#R # #\n" +
            "#    #\n" +
            "# ##\\#\n" +
            "#    #\n" +
            "######\n";

    static final String map2 =
            "######\n" +
            "#R # #\n" +
            "#    #\n" +
            "#### #\n" +
            "# \\  #\n" +
            "######\n";
    
    static final String map3 = 
            "######\n" +
            "#  # #\n" +
            "#R   #\n" +
            "# ##\\#\n" +
            "# \\  #\n" +
            "######\n";
    
    static final String map4 = 
            "######\n" +
            "####\\#\n" +
            "#R\\\\\\#\n" +
            "# ## #\n" +
            "#    #\n" +
            "######\n";
    
    static final String map5 =
            "######\n" +
            "#R # #\n" +
            "#    #\n" +
            "# ## #\n" +
            "#    #\n" +
            "######\n";

    @Test
    public void test1() throws Exception {
        
        MapExt map = new MapExt(MapLoader.loadMapFromString(map1));
        GlobalSearch globalSearch = new SimpleGlobalSearch();
        List<Position> lambdas = globalSearch.defineLambdasSequence(map, null);
        assertEquals(lambdas.size(), 1);
        Position lampdaPosition = lambdas.get(0);
        assertEquals(new Position(2,4), lampdaPosition);
    }

    @Test
    public void test2() throws Exception {

        MapExt map = new MapExt(MapLoader.loadMapFromString(map2));
        GlobalSearch globalSearch = new SimpleGlobalSearch();
        List<Position> lambdas = globalSearch.defineLambdasSequence(map, null);
        assertEquals(lambdas.size(), 1);
        Position lampdaPosition = lambdas.get(0);
        assertEquals( new Position(1,2), lampdaPosition);
    }
    
    @Test
    public void test3() throws Exception {

        MapExt map = new MapExt(MapLoader.loadMapFromString(map3));
        GlobalSearch globalSearch = new SimpleGlobalSearch();
        List<Position> lambdas = globalSearch.defineLambdasSequence(map, null);
        assertEquals(lambdas.size(), 2);
        Position lampdaPosition = lambdas.get(0);
        assertEquals( new Position(1,2), lampdaPosition);
        Position lampdaPosition2 = lambdas.get(1);
        assertEquals(new Position(2,4), lampdaPosition2);

       
    }
    
    @Test
     public void test4() throws Exception {

        MapExt map = new MapExt(MapLoader.loadMapFromString(map4));
        GlobalSearch globalSearch = new SimpleGlobalSearch();
        List<Position> lambdas = globalSearch.defineLambdasSequence(map, null);
        assertEquals(lambdas.size(), 4);
        Position lampdaPosition = lambdas.get(0);
        assertEquals( new Position(3,2), lampdaPosition);
        Position lampdaPosition2 = lambdas.get(1);
        assertEquals(new Position(3,3), lampdaPosition2);
        Position lampdaPosition3 = lambdas.get(2);
        assertEquals(new Position(3,4), lampdaPosition3);
        Position lampdaPosition4 = lambdas.get(3);
        assertEquals(new Position(4,4), lampdaPosition4);
    }
    @Test
    public void test5() throws Exception {

        MapExt map = new MapExt(MapLoader.loadMapFromString(map5));
        GlobalSearch globalSearch = new SimpleGlobalSearch();
        List<Position> lambdas = globalSearch.defineLambdasSequence(map, null);
        assertEquals(0, lambdas.size());
        
    }
}
