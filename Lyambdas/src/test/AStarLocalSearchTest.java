package test;

import art.intelligence.global.GlobalSearch;
import art.intelligence.global.SimpleGlobalSearch;
import art.intelligence.local.AStarLocalSearch;
import art.intelligence.local.LocalSearch;
import entity.MapExt;
import entity.Position;
import game.GameController;
import map.loader.MapLoader;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class AStarLocalSearchTest {

    static final String map1 =
            "######\n" +
            "#R # #\n" +
            "#    #\n" +
            "# ##\\#\n" +
            "#    #\n" +
            "######\n";

    static final String map2 =
            "######\n" +
            "#R # #\n" +
            "#    #\n" +
            "#### #\n" +
            "# \\  #\n" +
            "######\n";

    @Test
    public void test1() throws Exception {

        LocalSearch localSearch = new AStarLocalSearch();

        MapExt map = new MapExt(MapLoader.loadMapFromString(map1));
        Position robotPosition = GameController.detectRobotPosition(map);
        assertEquals(robotPosition, new Position(4,1));
        GlobalSearch globalSearch = new SimpleGlobalSearch();
        List<Position> lambdas = globalSearch.defineLambdasSequence(map, null);
        assertEquals(lambdas.size(), 1);
        Position lampdaPosition = lambdas.get(0);
        assertEquals(lampdaPosition, new Position(2,4));

        localSearch.rebuildPath(map, robotPosition, lampdaPosition);
    }

    @Test
    public void test2() throws Exception {

        LocalSearch localSearch = new AStarLocalSearch();

        MapExt map = new MapExt(MapLoader.loadMapFromString(map2));
        Position robotPosition = GameController.detectRobotPosition(map);
        assertEquals(robotPosition, new Position(4,1));
        GlobalSearch globalSearch = new SimpleGlobalSearch();
        List<Position> lambdas = globalSearch.defineLambdasSequence(map, null);
        assertEquals(lambdas.size(), 1);
        Position lampdaPosition = lambdas.get(0);
        assertEquals(lampdaPosition, new Position(1,2));

        localSearch.rebuildPath(map, robotPosition, lampdaPosition);
    }
}
