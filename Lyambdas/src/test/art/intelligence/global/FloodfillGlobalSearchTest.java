/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package art.intelligence.global;

import entity.Map;
import entity.MapExt;
import entity.Position;
import game.GameController;
import java.util.LinkedList;
import java.util.List;
import main.java.art.intelligence.global.FloodfillGlobalSearch;
import map.loader.MapLoader;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author REXIT
 */
public class FloodfillGlobalSearchTest {

    /**
     * Test of defineLambdasSequence method, of class FloodfillGlobalSearch.
     */
    
    String MapGiven1 = 
            "##########\n"+
            "#**..R...#\n"+
            "#\\\\..*\\..#\n"+
            "#\\..*\\...#\n"+
            "#\\.......#\n"+
            "##L#######\n";
    
    
    @Test
    public void testDefineLambdasSequence1() throws Exception 
    {
        System.out.println("Define Nearlest Lyambdas Position 1. Easy Work");
        Map map = new MapExt(MapLoader.loadMapFromString(MapGiven1));
        List<Position> exclude_l_l = new LinkedList<Position>();
        List<Position> lambdasSequence = null;
        GlobalSearch globalSearch = new FloodfillGlobalSearch();
        List<Position> expResult = new LinkedList<Position>();;
        Position expectedLyambdas = new Position(3, 6);
        expResult.add(0, expectedLyambdas);
        lambdasSequence = globalSearch.defineLambdasSequence(map, exclude_l_l);
        assertEquals(expResult, lambdasSequence);
        System.out.println(".....OK.....");
    }
    
    String MapGiven2 = 
            "###########\n"+
            "#....R....#\n"+
            "#..####...#\n"+
            "#..\\..\\...#\n"+
            "#.........#\n"+
            "##L########\n";
    
    
    @Test
    public void testDefineLambdasSequence2() throws Exception 
    {
        System.out.println("Define Nearlest Lyambdas Position 2. Another Easy Work");
        Map map = new MapExt(MapLoader.loadMapFromString(MapGiven2));
        List<Position> exclude_l_l = new LinkedList<Position>();
        List<Position> lambdasSequence = null;
        GlobalSearch globalSearch = new FloodfillGlobalSearch();
        List<Position> expResult = new LinkedList<Position>();;
        Position expectedLyambdas = new Position(2, 6);
        expResult.add(0, expectedLyambdas);
        lambdasSequence = globalSearch.defineLambdasSequence(map, exclude_l_l);
        assertEquals(expResult, lambdasSequence);
        System.out.println(".....OK.....");
    }
    
    String MapGiven3 = 
            "###########\n"+
            "#....R....#\n"+
            "#..\\.###..#\n"+
            "#....#\\...#\n"+
            "#....#....#\n"+
            "##L########\n";
    
    
    @Test
    public void testDefineLambdasSequence3() throws Exception 
    {
        System.out.println("Define Nearlest Lyambdas Position 3. Hided Lyambda");
        Map map = new MapExt(MapLoader.loadMapFromString(MapGiven3));
        List<Position> exclude_l_l = new LinkedList<Position>();
        List<Position> lambdasSequence = null;
        GlobalSearch globalSearch = new FloodfillGlobalSearch();
        List<Position> expResult = new LinkedList<Position>();;
        Position expectedLyambdas = new Position(3, 3);
        expResult.add(0, expectedLyambdas);
        lambdasSequence = globalSearch.defineLambdasSequence(map, exclude_l_l);
        assertEquals(expResult, lambdasSequence);
        System.out.println(".....OK.....");
    }

    String MapGiven4 = 
            "###########\n"+
            "#....R....#\n"+
            "#...\\.\\...#\n"+
            "#.........#\n"+
            "#.........#\n"+
            "##L########\n";

    
    
    @Test
    public void testDefineLambdasSequence4() throws Exception 
    {
        System.out.println("Define Nearlest Lyambdas Position 4. Hard Choise");
        Map map = new MapExt(MapLoader.loadMapFromString(MapGiven4));
        List<Position> exclude_l_l = new LinkedList<Position>();
        List<Position> lambdasSequence = null;
        GlobalSearch globalSearch = new FloodfillGlobalSearch();
        List<Position> expResult = new LinkedList<Position>();
        Position expectedLyambdas = new Position(3, 6);
        expResult.add(0, expectedLyambdas);
        for (int i=0; i < 10; i++)
        {
            System.out.println(".... "+i+" ....");
            lambdasSequence = globalSearch.defineLambdasSequence(map, exclude_l_l);
            assertEquals(expResult, lambdasSequence);
        }
        System.out.println(".....OK.....");
    }
    
    String MapGiven5 = 
            "###########\n"+
            "#.\\#.R.#.\\#\n"+
            "####...####\n"+
            "#.........#\n"+
            "#.........#\n"+
            "##L########\n";
            
    
    
    @Test
    public void testDefineLambdasSequence5() throws Exception 
    {
        System.out.println("Define Nearlest Lyambdas Position 5. Isolated Lyambdas");
        Map map = new MapExt(MapLoader.loadMapFromString(MapGiven5));
        List<Position> exclude_l_l = new LinkedList<Position>();
        List<Position> lambdasSequence = null;
        GlobalSearch globalSearch = new FloodfillGlobalSearch();
        List<Position> expResult = new LinkedList<Position>();
        lambdasSequence = globalSearch.defineLambdasSequence(map, exclude_l_l);
        assertEquals(expResult, lambdasSequence);
        System.out.println(".....OK.....");
    }
    
    /*
     * BUG Founded
     */
//    String MapGiven6 = 
//            "###########\n"+
//            "#.\\*.R.*.\\#\n"+
//            "#***...****\n"+
//            "#.........#\n"+
//            "#.........#\n"+
//            "##L########\n";
//            
//    
//    
//    @Test
//    public void testDefineLambdasSequence6() throws Exception 
//    {
//        System.out.println("Define Nearlest Lyambdas Position 6. Mined Lyambdas");
//        Map map = new MapExt(MapLoader.loadMapFromString(MapGiven5));
//        List<Position> exclude_l_l = new LinkedList<Position>();
//        List<Position> lambdasSequence = null;
//        GlobalSearch globalSearch = new FloodfillGlobalSearch();
//        List<Position> expResult = new LinkedList<Position>();
//        Position expectedLyambdas = new Position(4, 2);
//        expResult.add(0, expectedLyambdas);
//        lambdasSequence = globalSearch.defineLambdasSequence(map, exclude_l_l);
//        assertEquals(expResult, lambdasSequence);
//        System.out.println(".....OK.....");
//    }
}
