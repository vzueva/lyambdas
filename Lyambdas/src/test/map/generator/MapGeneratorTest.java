package map.generator;

import entity.MapExt;
import entity.RobotMovement;
import map.loader.MapLoader;
import org.junit.*;
import org.junit.Test;
import java.util.List;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author REXIT
 */
public class MapGeneratorTest 
{
    /*
     * �������� ��������� �����
     */

    String LeftGiven = 
            "##########\n"+
            "#**..R...#\n"+
            "#\\\\..*\\..#\n"+
            "#\\..*\\...#\n"+
            "#\\.......#\n"+
            "##L#######\n";
    String LeftResult = 
            "##########\n"+
            "#**.R ...#\n"+
            "#\\\\..*\\..#\n"+
            "#\\..*\\...#\n"+
            "#\\.......#\n"+
            "##L#######\n";

    @Test
    public void LeftMoveTest() throws Exception 
    {
        System.out.println("Validation Of Movement LEFT");
        RobotMovement robotMovement;
        //RobotMovement robotMovement2;
        MapExt mapG = new MapExt(MapLoader.loadMapFromString(LeftGiven));
        MapExt mapE = new MapExt(MapLoader.loadMapFromString(LeftResult));
        robotMovement = RobotMovement.LEFT;
        UpdateFlags result = MapGenerator.updateMap(mapG, robotMovement);
        String tmpG = mapG.toString();
        String tmpE = mapE.toString();
        assertEquals(mapE.toString(), mapG.toString());
        System.out.println(".....OK.....");
    }
    
    String UpGiven = 
            "##########\n"+
            "#**..R...#\n"+
            "#\\\\..*\\..#\n"+
            "#\\..*\\...#\n"+
            "#\\.......#\n"+
            "##L#######\n";
    String UpResult = 
            "##########\n"+
            "#**..R...#\n"+
            "#\\\\..*\\..#\n"+
            "#\\..*\\...#\n"+
            "#\\.......#\n"+
            "##L#######\n";

    @Test
    public void UpMoveTest() throws Exception 
    {
        System.out.println("Validation Of Movement UP");
        RobotMovement robotMovement;
        //RobotMovement robotMovement2;
        MapExt mapG = new MapExt(MapLoader.loadMapFromString(UpGiven));
        MapExt mapE = new MapExt(MapLoader.loadMapFromString(UpResult));
        robotMovement = RobotMovement.UP;
        UpdateFlags result = MapGenerator.updateMap(mapG, robotMovement);
        String tmpG = mapG.toString();
        String tmpE = mapE.toString();
        assertEquals(mapE.toString(), mapG.toString());
        System.out.println(".....OK.....");
    }
    
    String DownGiven = 
            "##########\n"+
            "#**..R...#\n"+
            "#\\\\..*\\..#\n"+
            "#\\..*\\...#\n"+
            "#\\.......#\n"+
            "##L#######\n";
    String DownResult = 
            "##########\n"+
            "#**.  ...#\n"+
            "#\\\\.R*\\..#\n"+
            "#\\..*\\...#\n"+
            "#\\.......#\n"+
            "##L#######\n";

    @Test
    public void DownMoveTest() throws Exception 
    {
        System.out.println("Validation Of Movement DOWN");
        RobotMovement robotMovement;
        //RobotMovement robotMovement2;
        MapExt mapG = new MapExt(MapLoader.loadMapFromString(DownGiven));
        MapExt mapE = new MapExt(MapLoader.loadMapFromString(DownResult));
        robotMovement = RobotMovement.LEFT;
        UpdateFlags result = MapGenerator.updateMap(mapG, robotMovement);
        robotMovement = RobotMovement.DOWN;
        UpdateFlags result2 = MapGenerator.updateMap(mapG, robotMovement);
        String tmpG = mapG.toString();
        String tmpE = mapE.toString();
        assertEquals(mapE.toString(), mapG.toString());
        System.out.println(".....OK.....");
    }

    String RightGiven = 
            "##########\n"+
            "#**..R...#\n"+
            "#\\\\..*\\..#\n"+
            "#\\..*\\...#\n"+
            "#\\.......#\n"+
            "##L#######\n";
    String RightResult = 
            "##########\n"+
            "#**.. R..#\n"+
            "#\\\\..*\\..#\n"+
            "#\\..*\\...#\n"+
            "#\\.......#\n"+
            "##L#######\n";

    @Test
    public void RightMoveTest() throws Exception 
    {
        System.out.println("Validation Of Movement RIGHT");
        RobotMovement robotMovement;
        //RobotMovement robotMovement2;
        MapExt mapG = new MapExt(MapLoader.loadMapFromString(RightGiven));
        MapExt mapE = new MapExt(MapLoader.loadMapFromString(RightResult));
        robotMovement = RobotMovement.RIGHT;
        UpdateFlags result = MapGenerator.updateMap(mapG, robotMovement);
        String tmpG = mapG.toString();
        String tmpE = mapE.toString();
        assertEquals(mapE.toString(), mapG.toString());
        System.out.println(".....OK.....");
    }
    
    /*
     * ���� �� ��������� ���� ������� ������, ������� �������� ������ � 1, ����������� ������, ������������� ����������� ������ ���
     * ��������� �������� ���������� �� 2 ����
     */
    String RockStestGiven = 
            "###########\n"+
            "#....R....#\n"+
            "#...* *...#\n"+
            "#...* *...#\n"+
            "#.* ... *.#\n"+
            "#.* ... *.#\n"+
            "#.........#\n"+
            "##L########\n";
    String RockStestResult = 
            "###########\n"+
            "#....R....#\n"+
            "#...   ...#\n"+
            "#...***...#\n"+
            "#.  ...  .#\n"+
            "#.**...**.#\n"+
            "#.........#\n"+
            "##O########\n";
    
    @Test
     public void RocksTest() throws Exception 
     {
        System.out.println("Rocks Test 1. Fall Of Rocks");
        RobotMovement robotMovement;
        //RobotMovement robotMovement2;
        MapExt mapG = new MapExt(MapLoader.loadMapFromString(RockStestGiven));
        MapExt mapE = new MapExt(MapLoader.loadMapFromString(RockStestResult));
        robotMovement = RobotMovement.WAIT;
        UpdateFlags result = MapGenerator.updateMap(mapG, robotMovement);
        result = MapGenerator.updateMap(mapG, robotMovement);
        String tmpG = mapG.toString();
        String tmpE = mapE.toString();
        assertEquals(mapE.toString(), mapG.toString());
        System.out.println(".....OK.....");
     }
    
    String RockStest2Given = 
            "###########\n"+
            "#    R    #\n"+
            "#         #\n"+
            "#         #\n"+
            "#         #\n"+
            "#     *   #\n"+
            "#     *   #\n"+
            "##L########\n";
    String RockStest2Result = 
            "###########\n"+
            "#    R    #\n"+
            "#         #\n"+
            "#         #\n"+
            "#         #\n"+
            "#         #\n"+
            "#     **  #\n"+
            "##O########\n";
    
    @Test
     public void RocksTest2() throws Exception 
     {
        System.out.println("Rocks Test 2. Rock On Rock");
        RobotMovement robotMovement;
        //RobotMovement robotMovement2;
        MapExt mapG = new MapExt(MapLoader.loadMapFromString(RockStest2Given));
        MapExt mapE = new MapExt(MapLoader.loadMapFromString(RockStest2Result));
        robotMovement = RobotMovement.WAIT;
        UpdateFlags result = MapGenerator.updateMap(mapG, robotMovement);
        result = MapGenerator.updateMap(mapG, robotMovement);
        String tmpG = mapG.toString();
        String tmpE = mapE.toString();
        for (int i=0; i<10; i++)
        {
            System.out.println(".... "+i+" ....");
            assertEquals(mapE.toString(), mapG.toString());
        }
        System.out.println(".....OK.....");
     }
    
    String RockStest3Given = 
            "###########\n"+
            "#    R    #\n"+
            "#         #\n"+
            "#         #\n"+
            "#         #\n"+
            "#     *   #\n"+
            "#     \\   #\n"+
            "##L########\n";
    String RockStest3Result = 
            "###########\n"+
            "#    R    #\n"+
            "#         #\n"+
            "#         #\n"+
            "#         #\n"+
            "#         #\n"+
            "#     \\*  #\n"+
            "##L########\n";
    
    @Test
     public void RocksTest3() throws Exception 
     {
        System.out.println("Rocks Test 3. Rock On Lyambda");
        RobotMovement robotMovement;
        //RobotMovement robotMovement2;
        MapExt mapG = new MapExt(MapLoader.loadMapFromString(RockStest3Given));
        MapExt mapE = new MapExt(MapLoader.loadMapFromString(RockStest3Result));
        robotMovement = RobotMovement.WAIT;
        UpdateFlags result = MapGenerator.updateMap(mapG, robotMovement);
        result = MapGenerator.updateMap(mapG, robotMovement);
        String tmpG = mapG.toString();
        String tmpE = mapE.toString();
        assertEquals(mapE.toString(), mapG.toString());
        System.out.println(".....OK.....");
     }
    
     String RockStest4Given = 
            "###########\n"+
            "#    R    #\n"+
            "#         #\n"+
            "#         #\n"+
            "#     *   #\n"+
            "#         #\n"+
            "#         #\n"+
            "##L########\n";
    String RockStest4Result = 
            "###########\n"+
            "#    R    #\n"+
            "#         #\n"+
            "#         #\n"+
            "#         #\n"+
            "#         #\n"+
            "#     *   #\n"+
            "##O########\n";
    
    @Test
     public void RocksTest4() throws Exception 
     {
        System.out.println("Rocks Test 4. Rock Fly");
        RobotMovement robotMovement;
        //RobotMovement robotMovement2;
        MapExt mapG = new MapExt(MapLoader.loadMapFromString(RockStest4Given));
        MapExt mapE = new MapExt(MapLoader.loadMapFromString(RockStest4Result));
        robotMovement = RobotMovement.WAIT;
        UpdateFlags result = MapGenerator.updateMap(mapG, robotMovement);
        result = MapGenerator.updateMap(mapG, robotMovement);
        String tmpG = mapG.toString();
        String tmpE = mapE.toString();
        assertEquals(mapE.toString(), mapG.toString());
        System.out.println(".....OK.....");
     }
    
    String RockStest5Given = 
            "###########\n"+
            "#         #\n"+
            "#         #\n"+
            "#         #\n"+
            "#         #\n"+
            "#....R*   #\n"+
            "#.....**  #\n"+
            "##O########\n";
    String RockStest5Result = 
            "###########\n"+
            "#         #\n"+
            "#         #\n"+
            "#         #\n"+
            "#         #\n"+
            "#.... R   #\n"+
            "#.....*** #\n"+
            "##O########\n";
    
    @Test
     public void RocksTest5() throws Exception 
     {
        System.out.println("Rocks Test 5. Moving The Rocks");
        RobotMovement robotMovement;
        //RobotMovement robotMovement2;
        MapExt mapG = new MapExt(MapLoader.loadMapFromString(RockStest5Given));
        MapExt mapE = new MapExt(MapLoader.loadMapFromString(RockStest5Result));
        robotMovement = RobotMovement.RIGHT;
        UpdateFlags result = MapGenerator.updateMap(mapG, robotMovement);
        robotMovement = RobotMovement.WAIT;
        result = MapGenerator.updateMap(mapG, robotMovement);
        result = MapGenerator.updateMap(mapG, robotMovement);
        String tmpG = mapG.toString();
        String tmpE = mapE.toString();
        assertEquals(mapE.toString(), mapG.toString());
        System.out.println(".....OK.....");
     }
     
    String RockStest6Given = 
            "###########\n"+
            "#         #\n"+
            "#         #\n"+
            "#         #\n"+
            "#         #\n"+
            "#...*R*   #\n"+
            "#.....**  #\n"+
            "##O########\n";
    String RockStest6Result = 
            "###########\n"+
            "#         #\n"+
            "#         #\n"+
            "#         #\n"+
            "#         #\n"+
            "#...*R*   #\n"+
            "#.....**  #\n"+
            "##O########\n";
    
    @Test
     public void RocksTest6() throws Exception 
     {
        System.out.println("Rocks Test 6. UnMoovable Rocks");
        RobotMovement robotMovement;
        //RobotMovement robotMovement2;
        MapExt mapG = new MapExt(MapLoader.loadMapFromString(RockStest6Given));
        MapExt mapE = new MapExt(MapLoader.loadMapFromString(RockStest6Result));
        robotMovement = RobotMovement.LEFT;
        UpdateFlags result = MapGenerator.updateMap(mapG, robotMovement);
        String tmpG = mapG.toString();
        String tmpE = mapE.toString();
        assertEquals(mapE.toString(), mapG.toString());
        System.out.println(".....OK.....");
     }

}
