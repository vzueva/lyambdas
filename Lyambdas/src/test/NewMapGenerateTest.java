/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import art.intelligence.global.GlobalSearch;
import art.intelligence.global.SimpleGlobalSearch;
import art.intelligence.local.AStarLocalSearch;
import art.intelligence.local.LocalSearch;
import art.intelligence.local.SimpleLocalSearch;
import entity.Map;
import entity.MapExt;
import entity.Position;
import entity.RobotMovement;
import game.GameController;
import map.generator.MapGenerator;
import map.generator.UpdateFlags;
import map.loader.MapLoader;
import org.junit.Test;
import java.util.List;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author REXIT
 */
public class NewMapGenerateTest {
    
    static final String map1 =
            "######\n" +
            "#R # #\n" +
            "#    #\n" +
            "# ##\\#\n" +
            "#    #\n" +
            "######\n";
    
    @Test
    public void test1() throws Exception {

        MapExt map = new MapExt(MapLoader.loadMapFromString(map1));
        Position robotPosition = GameController.detectRobotPosition(map);
        assertEquals( new Position(4,1), robotPosition);
        
    }
}
