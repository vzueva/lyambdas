package test;

import art.intelligence.global.GlobalSearch;
import art.intelligence.global.SimpleGlobalSearch;
import art.intelligence.local.AStarLocalSearch;
import art.intelligence.local.LocalSearch;
import entity.Map;
import entity.MapExt;
import entity.Position;
import game.GameController;
import map.loader.MapLoader;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class RobotPositionTest {

    static final String map1 =
            "######\n" +
            "#R # #\n" +
            "#    #\n" +
            "# ##\\#\n" +
            "#    #\n" +
            "######\n";

    static final String map2 =
            "######\n" +
            "#R # #\n" +
            "#    #\n" +
            "#### #\n" +
            "# \\  #\n" +
            "######\n";
    
    static final String map3 = 
            "######\n" +
            "#  # #\n" +
            "#R   #\n" +
            "# ##\\#\n" +
            "# \\  #\n" +
            "######\n";
    
    static final String map4 = 
            "######\n" +
            "####\\#\n" +
            "#R\\\\\\#\n" +
            "# ## #\n" +
            "#    #\n" +
            "######\n";
    
    static final String map5 =
            "######\n" +
            "#R # #\n" +
            "#    #\n" +
            "# ## #\n" +
            "#    #\n" +
            "######\n";

    @Test
    public void test1() throws Exception {

        MapExt map = new MapExt(MapLoader.loadMapFromString(map1));
        Position robotPosition = GameController.detectRobotPosition(map);
        assertEquals( new Position(4,1), robotPosition);
        
    }

    @Test
    public void test2() throws Exception {

        MapExt map = new MapExt(MapLoader.loadMapFromString(map2));
        Position robotPosition = GameController.detectRobotPosition(map);
        assertEquals(new Position(4,1), robotPosition);

        
    }
    
    @Test
    public void test3() throws Exception {

        MapExt map = new MapExt(MapLoader.loadMapFromString(map3));
        Position robotPosition = GameController.detectRobotPosition(map);
        assertEquals( new Position(3,1), robotPosition);
    }
    
    @Test
     public void test4() throws Exception {

        MapExt map = new MapExt(MapLoader.loadMapFromString(map4));
        Position robotPosition = GameController.detectRobotPosition(map);
        assertEquals( new Position(3,1), robotPosition);
    }
    @Test
    public void test5() throws Exception {

        MapExt map = new MapExt(MapLoader.loadMapFromString(map5));
        Position robotPosition = GameController.detectRobotPosition(map);
        assertEquals( new Position(4,1), robotPosition);
        
    }
}
